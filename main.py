from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import joblib

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
model = joblib.load('fishModel.pkl')

class FishMeasurement(BaseModel):
    Species: int
    Length1: float
    Length2: float
    Length3: float
    Height: float
    Width: float

class FishWeight(BaseModel):
    Weight: float

@app.post("/fish/predict/")
async def predict_fish_weight(fish: FishMeasurement):
    try:
        data = [[fish.Species, fish.Length1, fish.Length2, fish.Length3, fish.Height, fish.Width]]
        prediction = model.predict(data)
        
        return {prediction[0]}
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}
